enum class Color(val hex: String): Drawable {
    WHITE("#FFF"),
    BLACK("#000"),
    GRAY("#888"),
    RED("#F00"){
        override fun draw() {
            println("overridden draw fun")
        }
               },
    GREEN("#0F0"),
    BLUE("#00F");

    override fun draw() {
        println("draw color is $hex")
    }

    fun someMethod(){
        println("print something")
    }
}