fun main() {
    val myAuto = Car()
    println("Auto info")
    println("Brand ${myAuto.brand} model ${myAuto.model} color ${myAuto.color}")

    myAuto.start()
    myAuto.move()
    myAuto.move()
    myAuto.move()
    myAuto.move()
    myAuto.stop()

    val vw = Car3("VW", "Beetle", "Green")
    val lada = Car3("Lada", "Vesta", "Yellow")
    val bmw = Car3("BMW", "X6")
    printInfo(vw)
    printInfo(lada)
    printInfo(bmw)

    val renault = Car3("Renault" to "Duster", "Gray")
    println("Current speed - ${renault.currentSpeed}")
    //println("Current fuel - ${renault.fuelCount}")

    renault.accelerate(100.0)
    println("Current speed - ${renault.currentSpeed}")
    //println("Current fuel - ${renault.fuelCount}")

    renault.decelerate2(50.0)
    println("Current speed - ${renault.currentSpeed}")
    //println("Current fuel - ${renault.fuelCount}")
}

fun printInfo(car: Car3){
    println("Brand ${car.brand} model ${car.model} color ${car.color}")
}

