import javax.swing.AbstractListModel
import kotlin.random.Random

class Car {
    val brand = "Audi"
    val model = "A3"
    val color = "Red"

    fun move(){
        val distance = Random.nextInt(1, 10)
        println("car passed $distance km")
    }

    fun start(){
        println("car started")
    }

    fun stop(){
        println("car stopped")
    }
}

class Car2(brandArg: String, modelArg: String, colorArg: String) {
    val brand = brandArg
    val model = modelArg
    val color = colorArg

    fun move(){
        val distance = Random.nextInt(1, 10)
        println("car passed $distance km")
    }

    fun start(){
        println("car started")
    }

    fun stop(){
        println("car stopped")
    }
}

class Car3(val brand: String, val model: String, val color: String = "white") {

    // вторчиный конструктор
    constructor(descriptor: Pair<String, String>, color: String): this(descriptor.first, descriptor.second, color){
        println("secondary constructor")
    }

    init {
        println("Init car $model")
    }

    // к ним могут обращаться только члены класса. Чтоб нельзя было напрямую поменять извне.
    var currentSpeed = 0.0
        private set
    private var fuelCount = 0.0

    init{
        println("Second init")
    }

    fun accelerate(speed: Double){
        val needFuel = speed * 0.1
        if (fuelCount > needFuel){
        currentSpeed += speed
        useFuel(needFuel)}
        else println("Car has not enough fuel")
    }

    fun decelerate(speed: Double){
        val finalSpeed = currentSpeed - speed
        currentSpeed = if (finalSpeed < 0) 0.0 else finalSpeed
        currentSpeed -= speed
    }

    fun decelerate2(speed: Double){
        currentSpeed = maxOf(0.0, currentSpeed - speed)
    }

    // к нему могут обращаться только члены этого же класса
    private fun useFuel(fuelCount: Double){
        this.fuelCount -= fuelCount
    }


    fun move(){
        val distance = Random.nextInt(1, 10)
        println("car passed $distance km")
    }

    fun start(){
        println("car started")
    }

    fun stop(){
        println("car stopped")
    }

    companion object{
        const val wheelsCount = 4
        fun getCarClass(lenght: Double): String = when {
            lenght < 3.6 -> "A"
            lenght < 3.9 -> "B"
            lenght < 4.3 -> "C"
            lenght < 4.6 -> "D"
            lenght < 5 -> "E"
            else -> "F"
        }
    }
}