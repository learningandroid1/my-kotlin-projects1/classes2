fun main() {

}

// теперь у всех интов есть доп функция isEven()
fun Int.isEven(): Boolean {
    return this % 2 == 0
}

fun printNumber(number: Int){
    println(number.isEven())
    println(5.isEven())
    val a = Int.random()
 }

fun Int.Companion.random(): Int {
    return (System.currentTimeMillis() % 43).toInt()
}

val Int.isEven
    get() = this % 2 == 0