fun main() {
    println(genericFun(null))
    println(genericFun2(175))

    val object1 = Generic(5)

    val object2 = Generic("String")

    val object3 = Generic(Car())

    val person = Generic(Person("Nastya", "Pupkina"))
}

fun <T> genericFun(input: T): String {
    return input?.toString() ?: "Object is null"
}

fun <T> genericFun2(input: T): T {
    return input
}