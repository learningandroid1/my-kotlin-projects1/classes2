fun main() {
    println("Height - ${EiffelTower.height}")
    EiffelTower.turnOnBackLights()
    EiffelTower.turnOffBackLights()

    val tower1 = EiffelTower
    val tower2 = EiffelTower

    println("tower1 backlights on? - ${tower1.backLightsOn}")
    println("tower2 backlights on? - ${tower2.backLightsOn}")

    println("Wheels count - ${Car3.wheelsCount}")
    val classType = Car3.getCarClass(3.8)
    println(classType)
}