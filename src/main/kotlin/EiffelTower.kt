object EiffelTower {
    var backLightsOn: Boolean = false
    val height = 324

    fun turnOnBackLights(){
        println("BackLights On")
        backLightsOn = true
    }
    fun turnOffBackLights(){
        println("BackLights Off")
        backLightsOn = false
    }
}